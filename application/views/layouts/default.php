<!doctype html>

<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 

<head id="www-sitename-com" data-template-set="html5-reset">

	<meta charset="utf-8">
	
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title></title>
	
	<meta name="title" content="" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<!-- Google will often use this as its description of your page/site. Make it good. -->
	
	<meta name="google-site-verification" content="" />
	<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->

	<meta name="Copyright" content="" />
	
	<!--  Mobile Viewport Fix
	http://j.mp/mobileviewport & http://davidbcalhoun.com/2010/viewport-metatag
	device-width : Occupy full width of the screen in its current orientation
	initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
	maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width
	-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 

	<!-- Iconifier might be helpful for generating favicons and touch icons: http://iconifier.net -->
	<link rel="shortcut icon" href="<?php echo base_url('assets');?>/img/favicon.ico" />
	<!-- This is the traditional favicon.
		 - size: 16x16 or 32x32
		 - transparency is OK -->
		 
	<link rel="apple-touch-icon" href="<?php echo base_url('assets');?>/img/apple-touch-icon.png" />
	<!-- The is the icon for iOS's Web Clip and other things.
		 - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for retina display (IMHO, just go ahead and use the biggest one)
		 - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
		 - Transparency is not recommended (iOS will put a black BG behind the icon) -->
         
    <link href='http://fonts.googleapis.com/css?family=Open+Sans|Bree+Serif|Lobster|Devonshire' rel='stylesheet' type='text/css'/>
	
	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/reset.css" />
	<link rel="stylesheet" href="<?php echo base_url('assets');?>/css/style.css" />
	
	<!-- This is an un-minified, complete version of Modernizr. 
		 Before you move to production, you should generate a custom build that only has the detects you need. -->
	<script src="<?php echo base_url('assets');?>/js/modernizr-2.6.2.dev.js"></script>
    
    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script>window.jQuery || document.write("<script src='<?php echo base_url('assets');?>/js/jquery-1.9.1.min.js'>\x3C/script>")</script>
    
    <script src="<?php echo base_url('assets');?>/js/responsive-nav.js"></script>
	
	<!-- Application-specific meta tags -->
	<!-- Windows 8 -->
	<meta name="application-name" content="" /> 
	<meta name="msapplication-TileColor" content="" /> 
	<meta name="msapplication-TileImage" content="" />
	<!-- Twitter -->
	<meta name="twitter:card" content="">
	<meta name="twitter:site" content="">
	<meta name="twitter:title" content="">
	<meta name="twitter:description" content="">
	<meta name="twitter:url" content="">
	<!-- Facebook -->
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="" />

</head>
<?php
if($this->tank_auth->is_logged_in()) {
	$user = new stdClass();
	$user->username = $this->tank_auth->get_username();
}
else {
	$user = new stdClass();
	$user->username = 'Guest';
}
?>
<body>
    <div class="wrapper">
        <header id="header">
            <div id="header-wrapper">
                <div id="header-left">
                    <h1><a href="<?php echo base_url();?>">The Dawg's Blawg</a></h1>
                </div>
                <div id="header-right">
                    <nav>
                        <ul>
                            <li><a href="<?php echo base_url('blog');?>">Blog &raquo;</a></li>
                            <?php if($this->tank_auth->is_logged_in()) { ?>
                            <li><?php echo anchor('admin','Admin  &raquo;'); ?></li>
                            <li><a title="View Profile" href="<?php echo base_url('profile');?>"><?php //echo $user->username; ?>Profile &raquo;</a></li>
                            <li><a href="<?php echo base_url('auth/logout');?>">Logout &raquo;</a></li>
                            <?php } else { ?>
							<!--<li><?php //echo $user->username; ?></li>-->
                            <li><a href="<?php echo base_url('auth/register');?>">Register &raquo;</a></li>
                            <li><a href="<?php echo base_url('auth/login');?>">Login &raquo;</a></li>
                            <?php } ?>
                        </ul>
                        <a href="#" id="pull">Menu</a>
                    </nav>
                </div>
            </div>
        </header>
        <div id="content-wrapper">
            <div id="content-wrapper-outer">
                <div id="content-wrapper-inner">
                    <!--<h1><?php //echo anchor('','My Blog'); ?></h1>
                    <div id="loginDiv"><?php //echo anchor('admin','Admin'); ?></div>
                    <hr/>-->
                    <!--<?php //echo base_url();?> | <?php //echo "http://".$_SERVER['HTTP_HOST']."/codeigniter/"; ?>-->
                    <div class="columns"><?php echo $page_title; ?></div>
                    <!--<div id="video">
                        <div class="video-container">
                            <iframe width="610" height="343" src="//www.youtube.com/embed/xkD5mAWAyN4?rel=0&wmode=transparent" frameborder="0" wmode="Opaque" allowfullscreen></iframe>
                        </div>
                    </div>-->
                    <hr/>
                    
                    <?php echo $content; ?>
                            
                </div><!-- end content-wrapper-inner -->
            </div><!-- end content-wrapper-outer -->
        </div><!-- end content-wrapper -->
    </div><!-- end wrapper -->
    
    <!-- don't forget to concatenate and minify if needed -->
	<script src="<?php echo base_url('assets');?>/js/functions.js"></script>
    
</body>
</html>