
		<p>Welcome To The Admin Page <?php echo $username; ?>. All posts available for edit or deletion is listed below.</p>
        <br />
        <p><?php echo anchor('admin/create','Create New Post'); ?></p>
        <br />
		<?php
            $count = count($post['id']);
            for ($i=0; $i<$count; $i++) {
                echo '<div class="postDiv">';
                echo '<h4>'.$post['title'][$i];
                echo anchor('blog/view/'.$post['id'][$i],' [view]');
                echo anchor('admin/edit/'.$post['id'][$i],' [edit]');
                echo anchor('admin/delete/'.$post['id'][$i],' [delete]</h4>');
                echo '<p>'.$post['summary'][$i].'</p>';
                echo '</div>';
                if(($i+1) != $count) {
                    echo '<p>&nbsp;</p>';
                }
            }
        ?>