<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends MY_Controller {
	function __construct() {
		parent::__construct();
		
		$this->load->model('posts');
	}
	
	function index() {
		$this->view($postId=null);
	}

	function view($postId) {
		$data['post'] = $this->posts->get_posts($postId);
		if($postId !== null) {
			$this->add_view("content", "posts/single_view", $data);
			$this->add_content("page_title", "Blog Post ...");
		}
		else {
			$this->add_view("content", "posts/index", $data);
			$this->add_content("page_title", "Blog Posts ...");
		}
		$this->render();
	}
}