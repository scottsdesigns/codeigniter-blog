<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MY_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->library(array('tank_auth','form_validation'));
		$this->load->model('posts');
	}

	function index() {
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login');
		}
		else {
			$data['post'] = $this->posts->get_posts(null);
			$data['userId'] = $this->tank_auth->get_user_id();
			$data['username'] = $this->tank_auth->get_username();
	
			$this->add_view("content", "admin/index", $data);
			$this->add_content("page_title", "Admin Dashboard");
			$this->render();
		}
	}

	function create() {
		$data['userId'] = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();

		$this->form_validation->set_rules('title','title','required');
		$this->form_validation->set_rules('summary','summary','required');
		$this->form_validation->set_rules('content','content','required');

		if($this->form_validation->run()==FALSE) {
			$this->add_view("content", "admin/create", $data);
			$this->add_content("page_title", "Create Post");
			$this->render();
		}
		else {
			$data = $_POST;
			$this->posts->insert_post($data);
			redirect('admin');
		}
	}

	function edit($postId) {
		$data['userId'] = $this->tank_auth->get_user_id();
		$data['username'] = $this->tank_auth->get_username();
		$data['post'] = $this->posts->get_posts($postId);

		$this->form_validation->set_rules('title','title','required');
		$this->form_validation->set_rules('summary','summary','required');
		$this->form_validation->set_rules('content','content','required');

		if($this->form_validation->run()==FALSE) {
			$this->add_view("content", "admin/edit", $data);
			$this->add_content("page_title", "Edit Post");
			$this->render();
		}
		else {
			$data = $_POST;
			$this->posts->update_post($postId,$data);
			redirect('admin');
		}
	}

	function delete($postId) {
		$this->posts->delete_post($postId);
		redirect('admin');
	}

	function logout() {
		redirect ('/auth/logout');
	}

}