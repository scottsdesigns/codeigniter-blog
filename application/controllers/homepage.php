<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends MY_Controller {
	function __construct() {
		parent::__construct();
	}
	
	function index() {
		$data = array("text"=>"optional data variable if wanted for the views");
		$this->add_view("content", "homepage", $data); // put the "homepage" view in the "main" content area, passing in the $data array if wanted
		$this->add_content("page_title", "Welcome to my website"); // pass in the standard text content
		$this->render(); // render the page with the layout and the content
	}
}