// Browser detection for when you get desparate. A measure of last resort.
// http://rog.ie/post/9089341529/html5boilerplatejs

// var b = document.documentElement;
// b.setAttribute('data-useragent',  navigator.userAgent);
// b.setAttribute('data-platform', navigator.platform);

// sample CSS: html[data-useragent*='Chrome/13.0'] { ... }


// remap jQuery to $
(function($){


/* trigger when page is ready */
$(document).ready(function (){
	// your functions go here
});

$(function() {  
	var pull        = $('#pull');  
		menu        = $('nav ul');  
		menuHeight  = menu.height();  
  
	$(pull).on('click', function(e) {  
		e.preventDefault();  
		menu.slideToggle();  
	});  
});

$(window).resize(function(){  
	var w = $(window).width();  
	if(w > 320 && menu.is(':hidden')) {  
		menu.removeAttr('style');  
	}  
});


})(window.jQuery);